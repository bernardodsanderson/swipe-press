# swipe-press

[Swipe Press Demo](http://swipe-press.surge.sh)

> A Vue.js project /dist is the static version of the site

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader). Template from [https://github.com/vuejs-templates/pwa](https://github.com/vuejs-templates/pwa).

## How this project was built

Install vue-cli

`npm install -g vue-cli`

Make new project (unit and e2e tests not used in this)

`vue init pwa project-name`

Go into project

`cd project-name`

`npm install`

`npm run`


Your app will be in `/src` and after building the app, the files will be in `/dist`

`/static` folder is where you add local resources

`/build` folder has the service workers and various utilities

This app uses: 

- Material Design Light from [getmdl.io](https://www.getmdl.io)

- Local Storage database from [localStorageDB](https://github.com/knadh/localStorageDB)

- Manifest.json for app-like options [Chrome Developers](https://developer.chrome.com/extensions/manifest) [MDN Docs](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/manifest.json)

- VueTouch for touch events [VueTouch](https://github.com/vuejs/vue-touch)

- API request using Axios [Axios npmjs.com](https://www.npmjs.com/package/axios)

#### Tips and Tricks

- [How to achieve 60fps animations with css3](https://medium.com/outsystems-experts/how-to-achieve-60-fps-animations-with-css3-db7b98610108)