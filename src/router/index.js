import Vue from 'vue';
import Router from 'vue-router';
import SwipePress from '@/components/SwipePress';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'SwipePress',
      component: SwipePress
    },
    { 
      path: '/article/:created', 
      component: SwipePress
    }
  ]
})
